# -*- coding: utf-8 -*-
'''
@file Elevator_FSM.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an elevator's travel between the 
first and second floor.

The user has a button to select floor 1 or floor 2.

There are also proximity switches for when the elevator reaches the top and bottom floor.
'''
from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control the travel of an elevator between 
                the first and second floor.
    @details    This class implements a finite state machine to control the
                operation of the elevator.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT                      = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN               = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP                 = 2    
    
    ## Constant defining State 3
    S3_STOPPED_FLOOR1            = 3    
    
    ## Constant defining State 4
    S4_STOPPED_FLOOR2            = 4
        
    def __init__(self, interval, Button_1, Button_2, First, Second, Motor):
            '''
            @brief            Creates a TaskElevator object.
            @param Button_1   An object from class Button representing floor selection 1
            @param Button_2   An object from class Button representing floor selection 2
            @param First      An object from class Button representing the floor 1 proximity switch
            @param Second     An object from class Button representing the floor 2 proximity switch
            @param Motor      An object from class MotorDriver representing a DC motor.
            '''
            ## The state to run on the next iteration of the task.
            self.state = self.S0_INIT
            
            ## The button object used for selection of floor 1
            self.Button_1 = Button_1
            
            ## The button object used for selection of floor 2
            self.Button_2 = Button_2
            
            ## The button object used for the floor 1 proximity switch
            self.First = First
            
            ## The button object used for the floor 2 proximity switch
            self.Second = Second
            
            ## The motor object used to drive the elevator up and down
            self.Motor = Motor
            
            ## Counter that describes the number of times the task has run
            self.runs = 0
            
            ##  The amount of time in seconds between runs of the task
            self.interval = interval
            
            ## The timestamp for the first iteration
            self.start_time = time.time()
            
            ## The "timestamp" for when the task should run next
            self.next_time = self.start_time + self.interval
            
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:  
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Down()
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                if(self.Button_1.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_FLOOR1)
                    self.Motor.Down()
                print(str(self.runs) + ': State 1 - Going to Floor 1. ' + str(self.curr_time-self.start_time))
                       
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                self.transitionTo(self.S4_STOPPED_FLOOR2)
                self.Motor.Up()
                print(str(self.runs) + ': State 2 - Going to Floor 2. ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_STOPPED_FLOOR1):
                # Transition to state 1 if the left limit switch is active
                self.transitionTo(self.S2_MOVING_UP)
                if(self.Button_2.getButtonState()):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 3 - Elevator stopped on floor 1. ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_STOPPED_FLOOR2):
                # Run State 4 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                if(self.Button_1.getButtonState()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 4 - Elevator stopped on floor 2. ' + str(self.curr_time-self.start_time))
                
            else:
                # Invalid state code (error handling)
                print('Error.')
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary user to select floor 1 or floor 2 on the elevator. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))    
         
    def getButtonState(self):
        '''
        @brief        Gets the button state.
        # @details    Since there is no hardware attached this method
        #             returns a randomized True or False value.
        @return       A boolean representing the state of the button.
        '''
        
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                travel between the first and second floors.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the elevator up
        '''
        print('Motor moves elevator up.')
    
    def Down(self):
        '''
        @brief Moves the elevator down
        '''
        print('Motor moves elevator down.')
    
    def Stop(self):
        '''
        @brief Stops the motor
        '''
        print('Elevator stopped.')