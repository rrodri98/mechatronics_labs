'''
@file FSM_example.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary windshield
wiper.

The user has a button to turn on or off the windshield wipers.

There is also a limit switch at either end of travel for the wipers.
'''

from random import choice
import time

class TaskWindshield:
    '''
    @brief      A finite state machine to control windshield wipers.
    @details    This class implements a finite state machine to control the
                operation of windshield wipers.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0
    S1_STOPPED_AT_LEFT       = 1
    S2_STOPPED_AT_RIGHT      = 2
    S3_MOVING_LEFT           = 3
    S4_MOVING_RIGHT          = 4
    S5_DO_NOTHING            = 5
    
    def __init__(self, interval, GoButton, LeftLimit, RightLimit):
        '''
        @brief      Creates a TaskWindshield object.
        @param GoButton An object from class Button representing on/off
        @param LeftLimit An object from class Button representing the Left Limit Switch
        @param RightLimit An object from class Button representing the Right Limit Switch
        @param Motor An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
    
        ## The button object used for the go button
        self.GoButton = GoButton
    
        ## The button object used for the left limit
        self.LeftLimit = LeftLimit
        
        ## The button object used for the right limit
        self.RightLimit = RightLimit
        
        ## The motor object "wiping" the wipers
        self.Motor = Motor
      
        ## Counter that describes how many times the task has run
        self.runs = 0
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time() + self.interval
        
        ## The amount of time between runs of the task
        self.interval = interval
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
        
            if(self.state == self.S0_INIT):
                    # Run State 0 Code
                    self.transitionTo(self.S3_MOVING_LEFT)
                    self.Motor.Forward()
                    print(str(self.runs) + ': State 0' + str(time.time()))
                    
            elif(self.state == self.S1_STOPPED_AT_LEFT):
                # Run State 1 Code
                
                if(self.runs > 100):
                    self.transitionTo(self.S5_DO_NOTHING)
                
                if(self.goButton.getButtonState()):
                    self.transitionTo(self.S4_MOVING_RIGHT)
                    self.Motor.Reverse()
                print(str(self.runs) + ': State 1 ' + str(time.time()))
                
            elif(self.state == self.S2_STOPPED_AT_RIGHT):
                # Run State 2 Code
                self.transitionTo(self.S3_MOVING_LEFT)
                self.Motor.Forward()
                print(str(self.runs) + ': State 2 ' + str(time.time()))
                
            elif(self.state == self.S3_MOVING_LEFT):
                # Transition to state 1 if the left limit switch is active
                if(self.LeftLimit.getButtonState()):
                    self.transitionTo(self.S1_STOPPED_AT_LEFT)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 3 ' + str(time.time()))  
                 
            elif(self.state == self.S4_MOVING_RIGHT):
                # Run State 4 Code
                if(self.RightLimit.getButtonState()):
                    self.transitionTo(self.S2_STOPPED_AT_RIGHT)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 4 ' + str(time.time()))
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(time.time()))
            
            else:
                # Invalid state code (Error handling)
                pass
                
                self.runs += 1
                
                ## Specifying the next time the task will run
                self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        


class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])



class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the wipers
                wipe back and forth.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Forward(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving CCW')
    
    def Reverse(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor moving CW')
    
    def Stop(self):
        '''
        @brief Moves the motor forward
        '''
        print('Motor Stopped')

# This code would be put in main.py

# Creating objects to pass into task constructor
GoButton = Button('PB6')
LeftLimit = Button('PB7')
RightLimit = Button('PB8')
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskWindshield(0.1, GoButton, LeftLimit, RightLimit, Motor)

# Run the tasks in sequence over and over again
while True in range(1000000):
    task1.run()
    # task2.run()
    # task3.run()