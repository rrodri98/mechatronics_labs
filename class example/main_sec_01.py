'''
@file main_sec_01.py
'''

from FSM_example_sec_01 import Button, MotorDriver, TaskWindshield


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
GoButton = Button('PB6')
LeftLimit = Button('PB7')
RightLimit = Button('PB8')
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskWindshield(0.1, GoButton, LeftLimit, RightLimit, Motor)
task2 = TaskWindshield(0.1, GoButton, LeftLimit, RightLimit, Motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()
#    task3.run()