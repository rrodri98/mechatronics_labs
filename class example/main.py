# -*- coding: utf-8 -'*-
'''
@file main.py (Sets up drivers, sensors, create task objects, runs tasks)
'''

# import individual classes
from FSM_example import Button, MotorDriver, TaskWindshield

# Creating objects to pass into task constructor
GoButton = Button('PB6')
LeftLimit = Button('PB7')
RightLimit = Button('PB8')
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskWindshield(0.1, GoButton, LeftLimit, RightLimit, Motor)

# Run the tasks in sequence over and over again
for N in range(100000000):
    task1.run()
    # task2.run()
    # task3.run()