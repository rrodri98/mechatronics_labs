# -*- coding: utf-8 -*-
'''
@file main_Elevator.py

@mainpage

@section sec_intro Introduction
Most microcontrollers with a single core processor are unable to perform tasks simultaneously; however, they are capable of cooperative multitasking. The finite state machine for the elevator transports users between two floors. The resulting documentation of code may be found in the "Finite State Machine for Elevator" section.
The source code can be found at: https://bitbucket.org/rrodri98/mechatronics_labs/src/master/.
@section sec_fib Finite State Machine for Elevator 
The elevator is modeled by a finite state machine. The elevator passes through two floors depending on whether a fictitious button is pressed. See the finite state machine diagram for a reference.
@image html images.PNG
@author Rebecca Rodriguez
@date October 4, 2020

'''


from Elevator_FSM import Button, MotorDriver, TaskElevator

# Creating objects to pass into task constructor
Button_1 = Button('PB6')
Button_2 = Button('PB7')
First = Button('PB8')
Second = Button('PB9')
Motor = MotorDriver()

# Creating a task object using the button and motor objects above
task1 = TaskElevator(0.1, Button_1, Button_2, First, Second, Motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
