# -*- coding: utf-8 -*-
## @file Fib_Sequence.py
#  This method calculates a Fibonnaci number corresponding to a specific index.
#
#  The user supplies the index and the corresponding Fibonnaci number is returned. Index is valid for positive numbers only. 
#  The following reference on Stack Overflow was used: https://stackoverflow.com/questions/14907067/how-do-i-restart-a-program-based-on-user-input.
#  
#  @author Rebecca Rodriguez
#
#  @date September 24, 2020
#

#     '''This method calculates a Fibonnaci number corresponding to a specific index.
#     @param idx An integer specifying the index of the desired Fibonacci number.'''
while True:
    try:
        # The following prompt asks for the user to input an index.
        idx = input('This is a fibonacci sequence generator. Input an index for the desired Fibonacci number to continue (or "quit"): ')
        idx = int(idx)
        # An error message will occur if the user inputs a negative index.
        if idx < 0:
            print('Error. Please submit a positive integer index. ')
        # If the user enters a postive index the Fibonacci number will be determined using recursion.
        if idx >= 0:
            if idx == 0:
                print('Calculating Fibonacci number at  '
                   'index n = {:}.'.format(idx))
                print('Fibonacci number is 0. ')
            
            elif idx > 0:
                print('Calculating Fibonacci number at  '
                   'index n = {:}.'.format(idx))
                fibnumline = [0, 1] 
                for i in range(idx - 1):
                    fibnumline.append(fibnumline[len(fibnumline)-1] + fibnumline[len(fibnumline)-2])
                    fibnumline
                print('Fibonacci number is: ' + str(fibnumline[-1]))
                idx = input ('This is a fibonacci sequence generator. Input an index for the desired Fibonacci number to continue (or "quit"):  ')
                idx = int(idx)
                while idx > 0:
                    print('Calculating Fibonacci number at  '
                         'index n = {:}.'.format(idx))
                    fibnumline = [0, 1] 
                    for i in range(idx - 1):
                        fibnumline.append(fibnumline[len(fibnumline)-1] + fibnumline[len(fibnumline)-2])
                        fibnumline
                    print('Fibonacci number is: ' + str(fibnumline[-1]))
                    idx = input ('This is a fibonacci sequence generator. Input an index for the desired Fibonacci number to continue (or "quit"):  ')
                    idx = int(idx)
    # An error message will be displayed if the user inputs words or symbols as an index.
    except ValueError:
        print('Please enter a valid integer index. ')
    # The user has the option to quit the program by entering 'quit.'
    finally:
        if idx == 'quit':
            print('Program ended. Thank you. ')
            break        
        
     



