## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  The program generated is able to calculate Fibonacci numbers from a supplied user input. \n The Fibonacci Sequence can be found in nature; for example, the number of petals on a flower tend to follow the pattern of the Fibonacci Sequence [1]. \n [1] https://www.livescience.com/37470-fibonacci-sequence.html
#
#  @section sec_fib Fibonacci Function
#  The Fibonacci Number Generator only accepts integers as a valid input and will output an error message if negative numbers or words are supplied as the index instead. Furthermore, the user may supply large numbers for the index. The source code for this project may be found at: https://bitbucket.org/rrodri98/mechatronics_labs/src/master/.
#  The greatest challenge in this project was determining how the user would interface with the program. The program occurs repeatedly until the user enters 'quit.'
#
#  @author Rebecca Rodriguez
#  @date September 24, 2020
#