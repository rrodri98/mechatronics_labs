# -*- coding: utf-8 -*-

## @file FSM_virtualLED.py
#  A virtual LED is blinked on and off on a set interval. 
#
#  This task operates simultaneously with the task controlling the brightness of the LED.
#
#  @author Rebecca Rodriguez
#
#  @copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0. https://creativecommons.org/licenses/by-nc-sa/4.0/
#  @date October 19, 2020

import utime
# import time

class TaskLEDtoggle:
    '''
    @brief      A finite state machine to control the brightness of an LED.
    @details    This class implements a finite state machine to control the
                pulse width modulation of an onboard LED on the NUCLEO.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT    = 0    
    
    ## Constant defining State 1
    S1_LED_ON  = 1    
    
    ## Constant defining State 2
    S2_LED_OFF = 2 
    
    def __init__(self, interval):
        '''
        @brief            Creates a TaskLEDtoggle object.
        @param interval  The input to the module is the timespan between state changes.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        # self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The "timestamp" for when the task should run next
        # self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()
        # self.curr_time = time.time()
        
        if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Run ' + str(self.runs) + ': State 0 ')
                self.transitionTo(self.S1_LED_ON)
            
        elif(self.state == self.S1_LED_ON):
                # Run State 1 Code
                print('Run ' + str(self.runs) + ': State 1 - LED On ')
                self.transitionTo(self.S2_LED_OFF)
                
        elif(self.state == self.S2_LED_OFF):
                # Run State 2 Code
                print('Run ' + str(self.runs) + ': State 2 - LED Off ')
                self.transitionTo(self.S1_LED_ON)
                
        else:
                # Invalid state code (error handling)
                pass
            
        self.runs += 1
        # Specifying the next time the task will run
        # self.next_time += self.interval
        self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

# Following code moved to main file        
# task1 = TaskLEDtoggle(0.0001)
# for N in range(10000):
#     task1.run()
               
