# -*- coding: utf-8 -*-
'''
@file FSM_LED.py
@brief This finite state machine controls the brightness of an LED onboard the NUCLEO. The brightness of the LED is modeled by a sawtooth pattern. The brightness increases until the max value is reached, then it drops to zero.

@author Rebecca Rodriguez

@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0. https://creativecommons.org/licenses/by-nc-sa/4.0/
@date October 19, 2020
'''

import utime
# import time
import pyb

class TaskBlinkLED:
    '''
    @brief      A finite state machine to control the brightness of an LED.
    @details    This class implements a finite state machine to control the
                pulse width modulation of an onboard LED on the NUCLEO.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT    = 0    
    
    ## Constant defining State 1
    S1_LED_ON  = 1    
    
    ## Constant defining State 2
    S2_LED_OFF = 2 
    
    def __init__(self, interval):
        '''
        @brief           Creates a TaskBlinkLED object.
        @param interval  The input to the module is the timespan between state changes.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        # self.start_time = time.time()
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        # self.next_time = self.start_time + self.interval
        self.next_time = utime.ticks_add(self.start_time, self.interval) 
            
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        # self.curr_time = time.time()
        # Current time
        self.curr_time = utime.ticks_us()
        
        if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Run ' +str(self.runs) + ': State 0 - Initializing')
                self.transitionTo(self.S1_LED_ON)
                
        elif(self.state == self.S1_LED_ON):
                # Run State 1 Code
                pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
                tim2 = pyb.Timer(2, freq = 20000)
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
                brightness = 0
                while brightness <= 100:
                    t2ch1.pulse_width_percent(brightness)
                    print('Run ' +str(self.runs) + ': State 1 - LED PWM On')
                    brightness += 25
                    utime.sleep_us(200000)
                self.transitionTo(self.S2_LED_OFF)
            
        elif(self.state == self.S2_LED_OFF):
                # Run State 2 Code
                pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
                tim2 = pyb.Timer(2, freq = 20000)
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
                brightness = 0
                t2ch1.pulse_width_percent(brightness)
                print('Run ' +str(self.runs) + ': State 2 - LED PWM Off')
                self.transitionTo(self.S1_LED_ON)

        else:
                # Invalid state code (error handling)
                pass
            
        self.runs += 1
            # Specifying the next time the task will run
            # self.next_time += self.interval
        self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

            
# task2 = TaskBlinkLED(0.0001)
# for N in range(10000):
#     task2.run()



        
        

