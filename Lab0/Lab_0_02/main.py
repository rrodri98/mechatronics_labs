# -*- coding: utf-8 -*-
'''
@file main.py
This finite state machine controls the brightness of an LED onboard the NUCLEO using pulse width modulation while simultaneously running a virtual LED that blinks on and off.

@author Rebecca Rodriguez

@copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0.
@date October 19, 2020
'''

from FSM_LED import TaskBlinkLED
from FSM_virtualLED import TaskLEDtoggle

# Creating a task object for LED 
# Both tasks operate at the same frequency 
task1 = TaskBlinkLED(0.0001)
task2 = TaskLEDtoggle(0.0001)

# Run the tasks in sequence over and over again
for N in range(1000000):
    task1.run()
    task2.run()