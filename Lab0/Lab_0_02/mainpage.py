# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 17:37:47 2020

@author: Rebecca
"""

## @file mainpage.py
#  This finite state machine controls the brightness of an LED onboard the NUCLEO using pulse width modulation while simultaneously running a virtual LED that blinks on and off.
#
#  @author Rebecca Rodriguez
#
#  @copyright This file is licensed under the creative commons CC-BY-NC-SA 4.0.
#  @date October 19, 2020## @file main.py
