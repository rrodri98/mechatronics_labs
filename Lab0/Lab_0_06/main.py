'''@file main.py
'''

import pyb

class MotorDriver:
    ''' This class implements a motor driver for the ME 405 board.'''
    
    def __init__(self, pin_nSLEEP, pin_IN1, pin_IN2, tim):
        ''' Creates a motor driver by initializing GPIO pins and 
        turning the motor off for safety
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
                17 IN1_pin and IN2_pin. '''
        
        # # Set up pins as outputs for PWM
        self.pin_IN1 = pin_IN1
        self.pin_IN2 = pin_IN2
        self.pin_nSLEEP = pin_nSLEEP 
        
        # # Set up timer
        self.tim = tim
        self.pin_nSLEEP.low()
        print('Creating a motor driver. ')
        
    def enable(self):
        '''Sets n_SLEEP pin to high.'''
        self.pin_nSLEEP.high()
        print('Motor enabled. ')
        
    def disable(self):
        '''Sets n_SLEEP pin to low.'''
        self.pin_nSLEEP.low()
        print('Motor disabled. ')
        
    def set_duty(self, duty):
        '''This method sets the duty cycle for the motor. Positive or negative values may be inputs. Negative values will make the motor spin in reverse.'''
        while True:
            try:
                duty = input('Enter the duty cycle for the motor (or "quit"): ')
                duty = int(duty)
                
                print('Motor duty cycle sent to: ' + str(duty))
                # If the user enters a negative duty cycle the motor will spin in reverse.
                if duty > -100 and duty < 0:
                    # n_SLEEP must be set to High, IN1 must be set to high, and IN2 must be set to low
                    # self.pin_nSLEEP.high()
                    t_ch1.pulse_width_percent(0)
                    t_ch2.pulse_width_percent(duty * -1)
                    print('Motor in reverse. ')
                    
                # If the user enters a postive duty cycle the motor will spin forward.
                if duty > 0 and duty < 100:
                    # n_SLEEP must be set to High, IN1 must be set to low, and IN2 must be set to high
                    # self.pin_nSLEEP.high()
                    t_ch1.pulse_width_percent(duty)
                    t_ch2.pulse_width_percent(0)
                    print('Motor moving forward. ')
                 
                if duty == 0:
                    # self.pin_nSLEEP.high()
                    t_ch1.pulse_width_percent(duty)
                    t_ch2.pulse_width_percent(duty)
                    print('Motor stopped moving. ')
                     
        # An error message will be displayed if the user inputs words or symbols as an index.
            except ValueError:
                print('Please enter a valid integer from -100 to +100 for the duty cycle value. ')
        # The user has the option to quit the program by entering 'quit.'
            finally:
                if duty == 'quit':
                    print('No more inputs accepted. Thank you. ')
                    break
        
if __name__ == '__main__':
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    
    # Set up IN1 and IN2 to accept PWM
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)
       
    # Create the timer object used for PWM generation
    # Pins PB4 and PB5 use Timer 3
    tim = pyb.Timer(3, freq = 20000)  
    t_ch1 = tim.channel(1, pyb.Timer.PWM, pin = pin_IN1)
    t_ch2 = tim.channel(2, pyb.Timer.PWM, pin = pin_IN2)
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)

    # Enable the motor driver
    moe.enable()

    # Set the duty cycle to 10 percent
    moe.set_duty(10)



