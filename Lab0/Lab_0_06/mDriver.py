'''@file mDriver
'''

import pyb

class MotorDriver:
    ''' This class implements a motor driver for the ME 405 board.'''
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO pins and 
        turning the motor off for safety
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
                17 IN1_pin and IN2_pin. '''
        
        # Set up pins
        self.IN1_pin = pyb.Pin.cpu.A6
        self.IN2_pin = pyb.Pin.cpu.A7
        
        print('Creating a motor driver')
        
    def enable(self):
        '''Sets n_SLEEP pin to high.'''
        
        print('Creating a motor driver')
        
