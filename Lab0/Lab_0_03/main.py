'''
@file main.py
@brief       Main Script File
@details     Detailed doc for main.py 


@page page_LAB3 LAB0X03: Incremental Encoders
@ section page_Lab3_src Source Code Access
_______________________________bitbucket website
@author Rebecca Rodriguez

@date October 13, 2020

'''
    
import shares
import pyb
from encoder import EncoderDriver, EncoderTask
from user import UserTask


# Determine which pins to use
A6 = pyb.Pin.cpu.A6
A7 = pyb.Pin.cpu.A7 

## Creates encoder object specifying which pins the encoder is connected to, as well as which timing channel the encoder uses.
enc = EncoderDriver(A6, A7, 3)

task1 = EncoderTask(0.01, enc)
task2 = UserTask(0.01, enc)

## The task list contains the tasks to be run "round-robin" style
taskList = [task1,
            task2]

while True:
    for task in taskList:
        task.run()