# -*- coding: utf-8 -*-
#  Brief doc for the user module
#
#  Detailed doc for the user module
#
#  @author Rebecca Rodriguez
#
#  @date October 26, 2020

import shares
import utime
# UART is used to read and write characters
from pyb import UART


## A user task object
#
#  The user is able to specify any valid pin and timer combination to create many encoder objects.
#  @author Rebecca Rodriguez
#  @date October 13, 2020



class UserTask:
    '''
    @brief      A finite state machine to control the UserTask
    @details    Finite state macchine to control the
                ______________________________________.
               '''     
        
	## Constructor for encoder task
	#
	#  Detailed info on encoder task constructor
    ## Constant defining State 0 - Initialization
    S0_INIT          = 0    
    
    ## Constant defining State 1
    S1_WAIT_FOR_CMD  = 1  
    
    ## Constant defining State 2
    S2_WAIT_FOR_RESP = 2 
    
    # ## Constant defining State 3
    # S3_PRINT_POS     = 3 
    
    # ## Constant defining State 4
    # S4_PRINT_DELTA   = 4
    
	## Constructor for user task
	#
	#  Detailed info on user task constructor
    def __init__(self, interval, enc):
        '''
        @brief           Creates an encoder task object.
        @param interval  The input to the module is the timespan between state changes.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Serial port
        self.ser = UART(2)

	## 
	#
	#  Detailed info 
    def run(self):
        '''
        Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Run ' +str(self.runs) + ': State 0 - Initializing')
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
             if(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code
                # print('Run ' +str(self.runs) + ': State 1 - Wait for command')
                print('Input "z" to zero out the encoder position, "p" to print out the encoder position,or "d" to print out the encoder delta.')
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    shares.cmd = self.ser.readchar()   
            
             if(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                # print('Run ' +str(self.runs) + ': Waiting for response')
                if shares.resp:
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    self.ser.write(shares.resp)
                    # self.ser.writechar(shares.resp)
                    shares.resp = None
            
            #  if(self.state == self.S3_PRINT_POS):
            #     # Run State 3 Code
            #     print('Run ' +str(self.runs) + ': State 0 - Initializing')
            #     self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            # elif(self.state == self.S4_PRINT_DELTA):
            #     # Run State 4 Code
            #     self.update()
            #     print('Run ' +str(self.runs) + ': State 1 - Update Encoder Position')
            #     self.transitionTo(self.S1_WAIT_FOR_CMD)
            
        self.runs += 1
            
            # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState