## @file main.py
#  Brief doc for main.py
#
#  Detailed doc for main.py 
#
#  @author Rebecca Rodriguez
#
#  @date October 13, 2020
#
#

from motor import Motor

## A motor driver object
moto = motor.MotorDriver()

moto.set_duty_cycle(50)

# Creating a task object using the button and motor objects above
task1 = TaskWindshield(0.1, GoButton, LeftLimit, RightLimit, Motor)
task2 = TaskWindshield(0.1, GoButton, LeftLimit, RightLimit, Motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()