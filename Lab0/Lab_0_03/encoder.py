## @file encoder.py
#  Brief doc for encoder.py
#
#  Detailed doc for encoder.py 
#
#  @author Rebecca Rodriguez
#
#  @date October 13, 2020

import shares
import utime
import pyb

## An encoder driver object
#
#  Details
#  @author Rebecca Rodriguez
#  @date October 13, 2020

class EncoderDriver:
    
    '''
    @brief      A finite state machine to control the EncoderDriver.
    @details    This class implements a finite state machine to control the
                encoder driver.
    '''
	## Constructor for encoder driver
	#
	#  Detailed info on encoder driver constructor
    def __init__(self,pin1,pin2,timer):
        '''
        @brief           Creates an encoder driver object. The constructor creates the channel info, sets the timers, and zeros the variables.
        @param pin1
        @param pin2
        @param timer
        '''
        # Setup timer for encoder with no prescaler
        self.timer =  pyb.Timer(timer)
        
        # Set up period
        self.period = 65535
        
        # Period is max possible 16-bit number
        self.time = self.timer.init(prescaler = 0, period = self.period)
        
        # Set up pins
        self.pin1 = pyb.Pin.cpu.A6
        self.pin2 = pyb.Pin.cpu.A7
         
        # Setup timer channels
        self.CH1 = self.timer.channel(1, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.CH2 = self.timer.channel(2, pin = self.pin2, mode = pyb.Timer.ENC_AB)
        
        
        # Use a timer to "poll" both encoder channels
        # Check encoder value every rising edge of clock
        # Timer will automatically count up/down when a state change occurs
        # Counter needs to be fast enough to catch software transitions
        self.counter = self.timer.counter()
        
        # Setup initial position
        self.position = 0
        
        # Setup delta position
        self.delta_position = 0

	## Updates the encoder's position
	#
	#  Detailed info on encoder update method
    def update(self):
        '''
        Runs one iteration of the task. Returns the encoder's updated position.
        '''
        # Read timer count to get number of encoder ticks (need 2 valid readings from each state)
        self.previous_counter = self.counter
        self.counter = self.timer.counter()
        
        # Calculate delta assuming update has already occurred
        # Subtract old count to get delta
        self.delta = self.counter - self.previous_counter
        self.mag_delta = abs(self.delta)
        
        # Fix bad deltas if they exceed a certain amount
        if(self.mag_delta > self.period/2):
        # "Fix" delta- bad deltas overflow
            if(self.delta > 0):
                # Subtract delta by 8
                self.delta_position = self.delta - self.period               
            elif(self.delta < 0):
                # Add delta by 8 
                self.delta_position = self.delta + self.period              

        elif(self.mag_delta < self.period/2):
            # Good delta
            self.delta_position = self.delta
            
        else:
            self.delta_position = 0

        # Add change in position (delta_position) to original position
        self.position += self.delta_position		
    
    ## Obtain delta value
    def get_delta(self):
        return self.delta
    
    ## Gets the encoder's position
    def get_position(self):
        # Returns most recently updated position of the encoder
        return self.position

    ##Zeros out the encoder
    def set_position(self):
        # Sets the position of the encoder to zero
        self.position = 0
    
## An encoder task object
#
#  Details
#  @author Rebecca Rodriguez
#  @date October 26, 2020   
class EncoderTask(EncoderDriver):
    '''
    @brief      A finite state machine to control the EncoderTask.
    @details    This class implements a finite state machine to control the
                EncoderTask.
               '''             
	## Constructor for encoder task
	#
	#  Detailed info on encoder task constructor
    ## Constant defining State 0 - Initialization
    S0_INIT        = 0    
    
    ## Constant defining State 1
    S1_GET_UPDATE  = 1    
    
    def __init__(self, interval, enc):
        '''
        @brief           Creates an encoder task object.
        @param interval  The input to the module is the timespan between state changes.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1e6)
        
        ## The timestamp for the first iteration / Time in microseconds
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        pass
    
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
             if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Run ' +str(self.runs) + ': State 0 - Initializing')
                self.transitionTo(self.S1_GET_UPDATE)
                
             elif(self.state == self.S1_GET_UPDATE):
                # Run State 1 Code
                self.EncoderDriver.update()
                # self.update()
                print('Run ' +str(self.runs) + ': State 1 - Update Encoder Position')
                if shares.cmd:
                    shares.resp = self.returnData(shares.cmd)
                    shares.cmd = None
                self.transitionTo(self.S1_GET_UPDATE)
             else:
                # Invalid state code (error handling)
                pass
            
             self.runs += 1
            
             # Specifying the next time the task will run
             self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    def returnData(self, cmd):
        '''
        Performs the appropriate function and retrieves the appropriate data from the encoder.
        '''
        if cmd >= 90 or cmd <= 122:
            # If cmd = z or Z, zero the encoder position
            self.set_position()
            resp = self.position
            # print('Encoder position is set to zero.')
        elif cmd >= 80 and cmd <= 112:
            # If cmd = p or P, print the encoder position
            self.get_position()
            resp = self.position
        elif cmd >= 68 and cmd <= 100:
            # If cmd = d or D, print the encoder delta
            self.get_delta()
            resp = self.delta
        else:
            pass
            # resp = cmd
            
        return resp
        
# A6 = pyb.Pin.cpu.A6
# A7 = pyb.Pin.cpu.A7

# enc = EncoderDriver(A6, A7, 3)

# task1 = EncoderTask(0.01, enc)

# taskList = [task1]

# while True:
#     for task in taskList:
#         task.run()