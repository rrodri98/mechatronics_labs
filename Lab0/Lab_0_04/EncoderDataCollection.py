# -*- coding: utf-8 -*-
#  Brief doc for the user module
#
#  <<<<<<<<<<<<<<<<<<<<Needs to be completed>>>>>>>>>>>>>>>>>
#
#  @author Rebecca Rodriguez
#
#  @date November 2, 2020

#  <<<<<<<<<<<<<<<<<<<<In Progress>>>>>>>>>>>>>>>>>

import shares
import utime
# UART is used to read and write characters
from pyb import UART
# Import Encoder Driver


## A user task object
#
#  The user is able to specify any valid pin and timer combination to create many encoder objects.
#  @author Rebecca Rodriguez
#  @date October 13, 2020



class EncoderPos:
    '''
    @brief      A finite state machine to control the EncoderPos
    @details    Finite state macchine to control the
                data collection.
               '''     
        
	## Constructor for encoder task
	#
	#  Detailed info on encoder task constructor
    ## Constant defining State 0 - Initialization
    S0_INIT          = 0    
    
    ## Constant defining State 1
    S1_WAIT_FOR_CMD  = 1  
    
    ## Constant defining State 2
    S2_COLLECT_DATA = 2 
    
	## Constructor for user task
	#
	#  Detailed info on user task constructor
    def __init__(self, interval, enc):
        '''
        @brief           Creates an encoder task object.
        @param interval  The input to the module is the timespan between state changes.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
           
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  Sample at a rate of 5 Hz or 200 ms
        # Set interval as 1ms
        self.interval = int(interval)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Serial port
        self.ser = UART(2)
        
        # Timing Counter
        self.counter = 0
        
        # Empty array
        a = []

	## 
	#
	#  Detailed info 
    def run(self):
        '''
        Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
        
        
             if(self.state == self.S0_INIT):
                # Run State 0 Code - Do not accept any values
                print('Run ' +str(self.runs) + ': State 0 - Initializing')
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
             if(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code
                # print('Run ' +str(self.runs) + ': State 1 - Wait for command')
                while True:
                    if self.ser.any():
                        self.transitionTo(self.S2_COLLECT_DATA)
                        shares.cmd = self.ser.readchar()   
            
             if(self.state == self.S2_COLLECT_DATA):
                # Run State 2 Code
                # print('Run ' +str(self.runs) + ': Waiting for response')
                
                # User initiates data collection with inut "G"
                if shares.cmd == 71:
                # Counter will go up to 200 ms    
                    while self.counter < 200:
                        
                        self.dataGen()
                        
                        if shares.cmd == 85:
                            break
                        
                        self.counter += 1
                        pass
                    
                    self.transitionTo(self.S2_COLLECT_DATA)
                    pass
                
                # User terminates data collection with input "S" and goes back to state 1 waiting for a command
                if shares.cmd == 85:
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    pass
                
                if shares.resp:
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)
                    self.ser.write(shares.resp)
                    # self.ser.writechar(shares.resp)
                    shares.resp = None
            
        self.runs += 1
        
            
            # Specifying the next time the task will run
        self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState
        
    def dataGen(self):
        '''
        Obtains encoder position with respect to time.
        '''
        # Use array module
        # for x in xk:    
        #     
        # return a
        # pass