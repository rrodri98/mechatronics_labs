# -*- coding: utf-8 -*-
#  Brief doc for the user module
#
#  Detailed doc for the user module
#
#  @author Rebecca Rodriguez
#
#  @date November 2, 2020

import utime
import numpy as np
import matplotlib.pyplot as plt
import serial 



# Set up serial port
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)
    
# Send an ASCII character
def sendChar():
    # Needs to be completed
        inv = input('Give me a character. Input G to collect data and S to terminate data collection:  ')
        ser.write(str(inv).encode('ascii'))
        myval = ser.readline().decode('ascii')
        return myval
    
# Terminate script by calling sendChar function
for n in range(1):
        print(sendChar)
        
# Close serial port
ser.close()
    
# Need to plot Encoder Position vs Time
# Needs to be completed
plt.plot(tout, xout)
plt.xlabel("Time", fontsize = 18)
plt.ylabel("Encoder position", fontsize = 18)
plt.show()