## @file encoder.py
#  Brief doc for encoder.py
#
#  Detailed doc for encoder.py 
#
#  @author Rebecca Rodriguez
#
#  @date November 8, 2020

import shares
import pyb
from pyb import UART

## An encoder driver object
#
#  Details
#  @author Rebecca Rodriguez
#  @date November 8, 2020

class BLE_Driver:
    
    '''
    @brief      A finite state machine to control the BLEDriver.
    @details    This class implements a finite state machine to control the
                bluetooth module driver.
    '''
	## Constructor for bluetooth driver
	#
	#  Detailed info on bluetooth driver constructor
    def __init__(self,freq):
        '''
        @brief           Creates an encoder driver object. The constructor creates the channel info, sets the timers, and zeros the variables.
        @param freq
        '''
        
        # Initialize UART port
        self.uart = UART(3, baudrate = 9600)
        
        # Initialize LED as an ouput
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        # Create a timer object
        self.tim = pyb.Timer(2)
        

	## Turns the LED on or off
	#
	#  Detailed info on the LED_Switch method
    def LED_Switch(self):
        '''
        Runs one iteration of the task. Turns the LED on or off as specified by the user input.
        '''
        
        # Fix bad deltas if they exceed a certain amount
        if(self.mag_delta > self.period/2):
        # "Fix" delta- bad deltas overflow
            if(self.delta > 0):
                # Subtract delta by 8
                self.delta_position = self.delta - self.period               
            elif(self.delta < 0):
                # Add delta by 8 
                self.delta_position = self.delta + self.period              

        elif(self.mag_delta < self.period/2):
            # Good delta
            self.delta_position = self.delta
            
        else:
            self.delta_position = 0

        # Add change in position (delta_position) to original position
        self.position += self.delta_position		
    
    ## Obtain delta value
    def get_delta(self):
        return self.delta
    
    ## Gets the encoder's position
    def get_position(self):
        # Returns most recently updated position of the encoder
        return self.position

    ##Zeros out the encoder
    def set_position(self):
        # Sets the position of the encoder to zero
        self.position = 0